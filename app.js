const express = require('express')
const config = require('config')
const mongoose = require('mongoose')
const path = require('path')
const routeAuth = require('./routes/auth.routes')
const routeLink = require('./routes/link.routes')
const routeRedirect = require('./routes/redirect.routes')

const PORT = config.get('port') || 5000
const MONGO_URI = config.get('mongoUri')

const app = express()

app.use(express.json({ extended: true }))
app.use('/api/auth', routeAuth)
app.use('/api/link', routeLink)
app.use('/t', routeRedirect)

if (process.env.NODE_ENV === 'production') {
  app.use('/', express.static(path.join(__dirname, 'client', 'build')))
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
  })
  //видео остановилось на 3:08:52
}

async function start() {
  try {
    await mongoose.connect(MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    })
    app.listen(PORT, () => console.log(`App has been started on port ${PORT}...`))
  } catch (error) {
    console.log('Server error', error.message)
    process.exit(1)
  }
}

start()
